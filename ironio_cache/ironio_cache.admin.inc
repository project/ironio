<?php
/**
 * @file
 * ironio_cache.admin.inc
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds Iron.io cache settings to the Iron.io admin form.
 */
function ironio_cache_form_ironio_admin_form_alter(&$form, &$form_state, $form_id) {

  $form['cache_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['cache_settings']['ironio_cache_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Cache name'),
    '#default_value' => variable_get('ironio_cache_name', variable_get('site_name', 'Drupal')),
  );

  $form['#validate'][] = 'ironio_cache_admin_form_validate';

}

/**
 * Makes sure the cache will work with the entered settings.
 */
function ironio_cache_admin_form_validate($form, &$form_state) {
  $v = $form_state['values'];

  if (empty($v['ironio_token']) || empty($v['ironio_projectid'])) {
    return;
  }

  // Test to make sure the credentials work for the cache system.
  $ironio_cache = new IronCache(array(
    'token' => $v['ironio_token'],
    'project_id' => $v['ironio_projectid'],
  ), $v['ironio_cache_name']);

  try{

    // Try a bunch of stuff, most errors will throw an HTTP error.
    if (!$ironio_cache->put('ironio_module_test', 'test')) {
      form_set_error("", "Could not connect to the IronCache server.");
      return;
    }
    if ($ironio_cache->get('ironio_module_test')->value !== "test") {
      form_set_error("", "Error retrieving data from the IronCache server.");
      return;
    }
    if ($ironio_cache->delete('ironio_module_test')->msg !== "Deleted.") {
      form_set_error("", "Error deleting data from the IronCache server.");
      return;
    }

  }catch (Exception $e) {

    // This usually means the credentials are wrong.
    form_set_error("", t("Error connecting to IronCache: :message", array(":message" => $e->getMessage())));
    return;

  }

}
