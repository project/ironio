<?php
/**
 * @file
 * ironio_queue.admin.inc
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds custom validation handler for IronMQ.
 */
function ironio_queue_form_ironio_admin_form_alter(&$form, &$form_state, $form_id) {
  $form['#validate'][] = 'ironio_queue_admin_form_validate';
}

/**
 * Validates that the queue is working properly.
 */
function ironio_queue_admin_form_validate($form, &$form_state) {
  $v = $form_state['values'];

  if (empty($v['ironio_token']) || empty($v['ironio_projectid'])) {
    return;
  }

  // Test to make sure the credentials work for the cache system.
  $ironio_queue = new IronMQ(array(
    'token' => $v['ironio_token'],
    'project_id' => $v['ironio_projectid'],
  ));

  try{

    if (!$ironio_queue->postMessage("ironio_queue_test", "test message")) {
      form_set_error("", t("Error connecting to queue."));
      return;
    }

    if (!$result = $ironio_queue->getMessage("ironio_queue_test")) {
      form_set_error("", t("Error getting message from queue."));
      return;
    }

    if (!$ironio_queue->deleteMessage("ironio_queue_test", $result->id)) {
      form_set_error("", t("Error deleting message from queue."));
      return;
    }

  }catch (Exception $e) {

    form_set_error("", t("Error connecting to queue: :message", array(":message" => $e->getMessage())));
    return;

  }

}
