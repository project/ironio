<?php
/**
 * @file
 * ironio.admin.inc
 */

/**
 * Admin form for modifying Iron.io credentials.
 */
function ironio_admin_form($form, &$form_state) {

  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credentials'),
    '#description' => t('Your credentials can be found on your !link',
      array('!link' => l(t("Iron.io dashboard"), "https://hud.iron.io/dashboard",
        array("attributes" => array("target" => "_blank"))))),
  );

  $form['credentials']['ironio_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#default_value' => variable_get('ironio_token'),
  );

  $form['credentials']['ironio_projectid'] = array(
    '#type' => 'textfield',
    '#title' => t('Project ID'),
    '#default_value' => variable_get('ironio_projectid'),
  );

  return system_settings_form($form);
}

/**
 * Validates the token and project ID values are set.
 */
function ironio_admin_form_validate($form, &$form_state) {
  $v = $form_state['values'];
  if (!$v['ironio_token']) {
    form_set_error("ironio_token", "Token is required.");
  }
  if (!$v['ironio_projectid']) {
    form_set_error("ironio_projectid", "Project ID is required.");
  }
}
